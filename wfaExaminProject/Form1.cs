﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace wfaExaminProject
{
    public partial class Form1 : Form
    {


        // FOR GAME
        Button[] B;
        const int lengt = 25;
        // TAP ON LINE X/Y
        int count = 0;

        // FOR TIMER
        System.Timers.Timer timer;
        int hours = 0, minutes = 0, seconds = 0;

        public Form1()
        {
            

            // FOR GAME
            InitializeComponent();
            B = new Button[lengt];

            // row one
            B[0] = button1; 
            B[1] = button2;
            B[2] = button3;
            B[3] = button4;
            B[4] = button5;
            //row two
            B[5] = button6;
            B[6] = button7;
            B[7] = button8;
            B[8] = button9;
            B[9] = button10;
            //row three
            B[10] = button11;
            B[11] = button12;
            B[12] = button13;
            B[13] = button14;
            B[14] = button15;
            // row four
            B[15] = button16;
            B[16] = button17;
            B[17] = button18;
            B[18] = button19;
            B[19] = button20;
            button16.ForeColor = Color.Red;
            //row five
            B[20] = button21;
            B[21] = button22;
            B[22] = button23;
            B[23] = button24;
            B[24] = button25;

            randNums();
        }

        void randNums()
        {
            Random rnd = new Random();

            for (int i = 0; i < lengt; i++)
            {
                
                

                B[i].Text = rnd.Next(0, 3).ToString();
                
            }

            int randColorBtn = rnd.Next(0, lengt);
            B[randColorBtn].ForeColor = Color.Red;
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            Button btnClicked = (Button)sender;
            int n = (int)Convert.ToInt64(btnClicked.Tag.ToString());

            n--;

            int y = n / 5;
            int x = n - y * 5;

            var sumX = 0;
            var sumY = 0;



            // lvl 1 FOR x
            for (int i = 0; i < 5; i++)
            {


                if (y == 0)
                {
                    sumX += (int)Convert.ToInt64(B[i].Text.ToString());

                    if (B[i].ForeColor == Color.Red)
                    {
                        sumX -= (int)Convert.ToInt64(B[i].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                    
                }
                else if (y == 1)
                {
                    sumX += (int)Convert.ToInt64(B[i+5].Text.ToString());

                    if (B[i+5].ForeColor == Color.Red)
                    {
                        sumX -= (int)Convert.ToInt64(B[i+5].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                   
                }
                else if (y == 2)
                {
                    sumX += (int)Convert.ToInt64(B[i + 10].Text.ToString());

                    if (B[i+10].ForeColor == Color.Red)
                    {
                        sumX -= (int)Convert.ToInt64(B[i+10].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;

                    }
                    
                }
                else if (y == 3)
                {
                    sumX += (int)Convert.ToInt64(B[i + 15].Text.ToString());

                    if (B[i+15].ForeColor == Color.Red)
                    {
                        sumX -= (int)Convert.ToInt64(B[i+15].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                    
                }
                else if (y == 4 )
                {
                    sumX += (int)Convert.ToInt64(B[i+20].Text.ToString());

                    if (B[i+20].ForeColor == Color.Red)
                    {
                        sumX -= (int)Convert.ToInt64(B[i+20].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                   
                }

                if (y == 5)
                {
                    sumX += (int)Convert.ToInt64(B[i + 20].Text.ToString());
                    if (B[i+20].ForeColor == Color.Red)
                    {
                        sumX -= (int)Convert.ToInt64(B[i+20].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                   

                }

                
            }

            // lvl 3 FOR Y
            for (int i = 0; i < 25; i++)
            {
                if (x == 1)
                {
                    sumY += (int)Convert.ToInt64(B[i].Text.ToString());
                    if (B[i].ForeColor == Color.Red)
                    {
                        sumY -= (int)Convert.ToInt64(B[i].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                }
                else if (x == 2)
                {
                    sumY += (int)Convert.ToInt64(B[i+1].Text.ToString());
                    if (B[i + 1].ForeColor == Color.Red)
                    {
                        sumY -= (int)Convert.ToInt64(B[i + 1].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                }
                else if (x == 3)
                {
                    sumY += (int)Convert.ToInt64(B[i+2].Text.ToString());
                    if (B[i + 2].ForeColor == Color.Red)
                    {
                        sumY -= (int)Convert.ToInt64(B[i + 2].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }
                }
                else if (x == 4)
                {
                    sumY += (int)Convert.ToInt64(B[i+3].Text.ToString());
                    if (B[i + 3].ForeColor == Color.Red)
                    {
                        sumY -= (int)Convert.ToInt64(B[i + 3].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }

                }
                else if (x == 0)
                {
                    sumY += (int)Convert.ToInt64(B[i].Text.ToString());
                    if (B[i].ForeColor == Color.Red)
                    {
                        sumY -= (int)Convert.ToInt64(B[i].Text.ToString());
                        txtMistake.Text = "Red number";
                        count++;
                    }

                }
                // error
              

                i += 4;
            }

            txtMistake.Text = count.ToString();

            txtSumX.Text = sumX.ToString();
            txtSumY.Text = sumY.ToString();

        }

        void checkOnMaxMin(int sumX, int minX)
        {
            if (sumX > minX)
            {
                txtLose.Text = "You lost";
            }
            else
            {
                txtLose.Text = "You Win";
            }


        }

        // Timer Logic

        private void btnStart_Click(object sender, EventArgs e)
        {
            txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                           minutes.ToString().PadLeft(2, '0'),
                                                           seconds.ToString().PadLeft(0, '0'));

            timer.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            hours = 0;
            minutes = 0;
            seconds = 0;

            txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));

            timer.Stop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000; // 1 sec
            timer.Elapsed += OnTimeEvents;
        }

        private void OnTimeEvents(object sender, ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                seconds += 1;
                if (seconds == 60)
                {
                    seconds = 0;
                    minutes += 1;
                }
                if (minutes == 60)
                {
                    minutes = 0;
                    hours += 1;
                }
                txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));
            }));
        }
    }
}
